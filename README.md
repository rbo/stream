Il player e' uno dei punti d'accesso primari per l'ascolto della radio si trova all'indirizzo https://stream.radioblackout.org o anche alla pagina https://radioblackout.org/streaming.

E' una pagina statica i cui sorgenti si possono trovare [qui](https://0xacab.org/rbo/stream).

Sembra banale ma si preoccupa di alcune cose non scontate:

Se il dispositivo lo supporta usa lo stream .ogg altrimenti il classico mp3.
Se l'utente (dal sistema operativo) sceglie di usare poco traffico (ad es. perche' ha il traffico a pagamento) usa lo streaming a bassa qualita'.
In caso di disconnessione / errore riprova a collegarsi ogni tot.
Supporta anche i controlli dal sistema operativo (play/pause e immagine del media).


TODO:
- [ ] valutare [howler.js](https://github.com/goldfire/howler.js)
- [ ] come gestire i metadati dallo streaming? [icecast-metadata-player](https://github.com/eshaz/icecast-metadata-js/tree/master/src/icecast-metadata-player) ?
